
use std::io;
use std::fmt;

use std::sync::mpsc;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::TryRecvError;
use std::{thread, time};



/// A text or background color.
#[derive(Debug, Default, Copy, Clone)]
pub enum Color {
    #[default]
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
}

#[derive(Default, Copy, Clone)]
#[derive(Debug)]
pub struct Style {
    /// The text (or foreground) color.
    pub text: Option<Color>,
    /// The background color.
    pub background: Option<Color>,
    /// True if the text should have increased intensity.
    pub intense: Option<bool>,
}

impl Style {
    /// Returns a `Style` with all fields set to their defaults.
    pub fn new() -> Style {
        Style::default()
    }

    /// Sets the text color.
    pub fn text(&mut self, text: Color) -> &mut Style {
        self.text = Some(text);
        self
    }

    /// Sets the background color.
    pub fn background(&mut self, background: Color) -> &mut Style {
        self.background = Some(background);
        self
    }

    /// Sets the text intensity.
    pub fn intense(&mut self, intense: bool) -> &mut Style {
        self.intense = Some(intense);
        self
    }
}




/// An `encode::Write`r that wraps an `io::Write`r, emitting ANSI escape codes
/// for text style.
#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct AnsiWriter<W>(pub W);

impl<W: io::Write> io::Write for AnsiWriter<W> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.0.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.flush()
    }

    fn write_all(&mut self, buf: &[u8]) -> io::Result<()> {
        self.0.write_all(buf)
    }

    fn write_fmt(&mut self, fmt: fmt::Arguments) -> io::Result<()> {
        self.0.write_fmt(fmt)
    }
}

/// A trait for types that an `Encode`r will write to.
///
/// It extends `std::io::Write` and adds some extra functionality.
pub trait ConsoleWrite: io::Write {
    /// Sets the output text style, if supported.
    ///
    /// `Write`rs should ignore any parts of the `Style` they do not support.
    ///
    /// The default implementation returns `Ok(())`. Implementations that do
    /// not support styling should do this as well.
    #[allow(unused_variables)]
    fn set_style(&mut self, style: &Style) -> io::Result<()> {
        Ok(())
    }
}

impl<'a, W: ConsoleWrite + ?Sized> ConsoleWrite for &'a mut W {
    fn set_style(&mut self, style: &Style) -> io::Result<()> {
        <W as ConsoleWrite>::set_style(*self, style)
    }
}

impl<W: io::Write> ConsoleWrite for AnsiWriter<W> {
    fn set_style(&mut self, style: &Style) -> io::Result<()> {
        let mut buf = [0; 12];
        buf[0] = b'\x1b';
        buf[1] = b'[';
        buf[2] = b'0';
        let mut idx = 3;

        if let Some(text) = style.text {
            buf[idx] = b';';
            buf[idx + 1] = b'3';
            buf[idx + 2] = color_byte(text);
            idx += 3;
        }

        if let Some(background) = style.background {
            buf[idx] = b';';
            buf[idx + 1] = b'4';
            buf[idx + 2] = color_byte(background);
            idx += 3;
        }

        if let Some(intense) = style.intense {
            buf[idx] = b';';
            if intense {
                buf[idx + 1] = b'1';
                idx += 2;
            } else {
                buf[idx + 1] = b'2';
                buf[idx + 2] = b'2';
                idx += 3;
            }
        }
        buf[idx] = b'm';
        self.0.write_all(&buf[..=idx])
    }
}

fn color_byte(c: Color) -> u8 {
    match c {
        Color::Black => b'0',
        Color::Red => b'1',
        Color::Green => b'2',
        Color::Yellow => b'3',
        Color::Blue => b'4',
        Color::Magenta => b'5',
        Color::Cyan => b'6',
        Color::White => b'7',
    }
}

#[derive(Debug)]
struct TokenMatch {
    token: String
}
fn token_match<'a>(txt: &'a str, token: &'a TokenMatch) -> bool {
    txt.to_ascii_lowercase().contains(&token.token)
}

#[derive(Debug)]
struct RenderPair {
    style: Style,
    token: TokenMatch,
}

#[derive(Debug)]
struct RenderMap{
    render_pairs: Vec<RenderPair>
}
impl Default for RenderMap {
    fn default() -> Self {
        let v = vec![

            RenderPair { style: *Style::new().text(Color::Red).intense(true),     token: TokenMatch {token: "error".to_string()}},
            RenderPair { style: *Style::new().text(Color::Magenta), token: TokenMatch {token: "warn".to_string()}},
            RenderPair { style: *Style::new().text(Color::Green),   token: TokenMatch {token: "debug".to_string()}},
            RenderPair { style: *Style::new().text(Color::Cyan),    token: TokenMatch {token: "info".to_string()}},
        ];
        RenderMap { render_pairs: v }
    }
}

impl RenderMap {
    fn color(&self, text: &str) -> Style {
        self.render_pairs.iter().find_map(|x| if token_match(text, &x.token) {Some(x.style)} else { None } ).unwrap_or_default()
    }
}


fn render_line<'w, W: ConsoleWrite >(w: &'w mut W, line: &'w str, render_map: &'w RenderMap ) -> std::io::Result<()> {
    let sty = render_map.color(line);
    w.set_style(&sty)?;
    w.write_all(line.as_bytes())?;
    w.flush()
}

fn spawn_stdin_channel() -> Receiver<String> {
    let (tx, rx) = mpsc::channel::<String>();
    thread::spawn(move || loop {
        let mut buffer = String::new();
        io::stdin().read_line(&mut buffer).unwrap();
        tx.send(buffer).unwrap();
    });
    rx
}

fn main() -> std::io::Result<()> {
    let stdin_channel = spawn_stdin_channel();

    let stdout = io::stdout();
    let mut w = AnsiWriter(stdout.lock());

    let render_map = RenderMap::default();

    loop {
        match stdin_channel.try_recv() {

            Ok(line) => { 
                render_line(&mut w, &line, &render_map)?;
            }
            Err(TryRecvError::Empty) => { /* println!("Channel empty") */ } ,
            Err(TryRecvError::Disconnected) => panic!("Channel disconnected"),
        }
        thread::sleep(time::Duration::from_millis(10));
    }
}

#[cfg(test)]
#[test]
fn test_renders() {
    todo!()
}
