# Beautrs

beautrs is a simple highlighter for logs (or whatever) to make it more readable in the console. There are many other similar utilities. Currently this only accepts input from stdin. 

It is fairly dumb at the moment and only colorized based on keywords, and the keywoards are hardcoded, along with their respective colors:

 * error -> Red
 * warn -> Magenta
 * debug -> green
 * info -> Cyan
 
I hope this gets expanded in the future, but for now this tool does what i need it to do.

It was inspired by the color outout from `log4rs` which i'd like to have while tailing any logfile. 

I used the coloring code from `log4rs`, namely i used their [encoder](https://github.com/estk/log4rs/tree/master/src/encode/writer) code to write color lines to the console. 

## Install

    cargo install --git https://gitlab.com/noyez/beautrs

or

    git clone https://gitlab.com/noyez/beautrs
    cd beautrs
    cargo install --path .

## Usage

    sudo tail /var/log/syslog | beautrs



